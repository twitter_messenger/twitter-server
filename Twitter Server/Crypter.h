#ifndef CRYPTER_H
#define CRYPTER_H

#include <QObject>

class Crypter : public QObject
{
    Q_OBJECT
public:
    static QString cryptString(QString key,QString toCrypt)
    {
        QString resultString = "";
        for ( int i = 0; i < toCrypt.length(); i++)
        {
            resultString.append(QString(QChar(toCrypt[i]).unicode()^QChar(key[keyIndex(key,i)]).unicode()));
        }
        return resultString;
    }
    static QString decryptString(QString key,QString toDecrypt)
    {
        QString resultString = "";
        for ( int i = 0; i < toDecrypt.length(); i++)
        {
            resultString.append(QString(QChar(toDecrypt[i]).unicode()^QChar(key[keyIndex(key,i)]).unicode()));
        }
        return resultString;
    }
private:
    Crypter(QObject *parent = 0);
    static int keyIndex(QString key,int index)
    { 
        int len = key.length();
        int multiple = index / len;
        if ( index >=  len ) {
            return index - (len * multiple);
        }
        return index;
    }
};

#endif // CRYPTER_H
