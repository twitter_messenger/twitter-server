#pragma once
#include <QNetworkRequest>
#include <qnetworkreply.h>
#include <QHttpPart>
#include <qhttpmultipart.h>
#include <qbytearray>
#include <qobject.h>
#include <QEventLoop>


/*!
	�����, ��� �������� ��������

	TODO :
		��������� ������
 */
class Request : public QObject
{
	Q_OBJECT
public:
	Request();
	~Request();	
	
	void setHttpPart(QHttpPart * httpPart);
	void setURL(QByteArray url);
	void setHeader(QByteArray headerName,QByteArray value);	

	QByteArray get();
	QByteArray post();					

	QByteArray requestString;			// ���������� �������

private:
	QNetworkAccessManager * manager;	// �������� ���������
	QNetworkRequest request;			// ������
	QByteArray result;					// ����� �������
	QEventLoop * loop;					// ���� �������������
	QHttpMultiPart * multipart;

private slots:
	void getResponse(QNetworkReply*);
};

