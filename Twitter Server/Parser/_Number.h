#pragma once
#include "Value.h"
#include<math.h>

class _Number : public Value
{
public:
	_Number(void);
	~_Number(void);
	float value;
	_Number* parse(std::string & string, int & index);
	std::string toString();
	bool isInt();
	void print(int level=0);
};

