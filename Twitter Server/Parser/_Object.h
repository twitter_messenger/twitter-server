#pragma once
#include "Value.h"
#include "_Mass.h"
#include "_String.h"
#include "_Bool.h"
#include "_Number.h"
#include "_Null.h"
#include <list>
#include <vector>
class _Object : public Value
{
public:
	_Object(void);
	~_Object(void);
	
	std::list <std::pair<char*,Value*>> value;
	_Object* parse(std::string & string,int & index);	
	void print(int level=0);

	Value * getValue(char * key)
	{
		for (auto i=value.cbegin();i!=value.cend();i++)
		{
			if (strcmp(i->first,key)==0)
				return i->second;
		}
		return NULL;
	}

	bool contains(const char * key)
	{
		for (auto i=value.cbegin();i!=value.cend();i++)
		{
			if (strcmp(i->first,key)==0)
				return true;
		}
		return false;
	}
};

