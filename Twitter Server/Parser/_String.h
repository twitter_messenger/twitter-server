#pragma once
#include "Value.h"
#include <ostream>

class _String : public Value
{
public:
	_String(void);
	~_String(void);
	char* value;
	_String * parse(std::string & string,int & index);
	
	void print(int level=0);
};

