#include "_Object.h"
#include <ctime>

_Object::_Object(void)
{
	type=_object;
}


_Object::~_Object(void)
{

}

void _Object::print(int level)
{
	std::string tab(level,'\t');

	for (auto i=value.cbegin();i!=value.cend();i++)
	{
		printf("\n%s%s : ",tab.c_str(),i->first);
		i->second->print(level+1);
	}
}

_Object* _Object::parse(std::string & string, int & index)
{
	do
    {
		
		while(string[index]=='{' || isspace(string[index]))
		{
			index++;
		}
		index++;
		bool flag=true;
		int in=index;
		for(int i=index;flag!=false;i++)
		{
			if(string[i]=='"' && string[i-1]!='\\')
			{
				flag=false;
				index++;
			}
			else
			{
				index++;
			}
		}
		char* key=new char[index-in+1];
		flag=true;
		int i=0;
		for(i=0;in+i<index-1;i++)
		{
			key[i]=string[in+i];
		}
		key[i]='\0';
		while(isspace(string[index]) || string[index]==':')
		{
			index++;
		}
        if(string[index]=='{')
		{
			_Object* obj=new _Object();
			index++;
			obj->parse(string,index);
			this->value.push_back(std::pair<char*,Value*>(key,obj));
		}
		else if(string[index]=='[')
		{
			_Mass* mass=new _Mass();
			mass->parse(string,index);
			this->value.push_back(std::pair<char*,Value*>(key,mass));
		}
		else if(string[index]=='t' || string[index]=='f')
		{
			_Bool* Bool=new _Bool();
			Bool->parse(string,index);
			this->value.push_back(std::pair<char*,Value*>(key,Bool));
		}
		else if(string[index]=='n')
		{
			_Null* Null=new _Null();
			Null->parse(string,index);
			this->value.push_back(std::pair<char*,Value*>(key,Null));
		}
		else if(isdigit(string[index]) || string[index]=='-')
		{
			_Number* num=new _Number();
			num->parse(string,index);
			this->value.push_back(std::pair<char*,Value*>(key,num));
		}
		else if (string[index]=='"')
		{
			_String* str=new _String();
			str->parse(string,index);
			this->value.push_back(std::pair<char*,Value*>(key,str));
		}
		while(isspace(string[index]) || string[index]==',')
		{
			index++;
		}		
	}
	while(string[index] != '}');
	index++;
	/*if(index!=string.length())
	{
		while(isspace(string[index]))
		{
			index++;
		}
	}*/
	return this;
}
