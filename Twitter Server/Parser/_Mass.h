#pragma once
#include "Value.h"
#include "_Object.h"
#include "_Mass.h"
#include "_String.h"
#include "_Bool.h"
#include "_Number.h"
#include <list>
class _Mass : public Value
{
public:
	_Mass(void);
	~_Mass(void);
	std::list <Value*> value; 
	_Mass* parse(std::string & string, int & index);	

	void print(int level=0);
};

