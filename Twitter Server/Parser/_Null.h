#pragma once
#include "Value.h"
class _Null : public Value
{
public:
	_Null(void);
	~_Null(void);
	_Null* parse(std::string & string, int & index);
	std::string toString();
	void print(int level=0);
};