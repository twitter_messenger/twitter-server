#include "_String.h"


_String::_String(void)
{
	type=_string;
}


_String::~_String(void)
{
}

void _String::print(int level)
{
	std::string tab(level,'\t');
	printf("\n%s%s",tab.c_str(),value);	
}

_String* _String::parse(std::string & string, int & index)
{
	index++;
	bool flag=true;
	int in=index;
	for(int i=index;flag!=false;i++)
	{
		if(string[i]=='"' && string[i-1]!='\\')
		{
			flag=false;
			index++;
		}
		else
		{			
			index++;
		}
	}
	char* key=new char[index-in+1];
	flag=true;
	int i=0;
	for(i=0;in+i<index-1;i++)
	{
		key[i]=string[in+i];
	}
	key[i]='\0';
	this->value=key;
	return this;
}