#include "_Bool.h"
_Bool::_Bool(void)
{
	type=_bool;
}


_Bool::~_Bool(void)
{
}

_Bool* _Bool::parse(std::string & string, int & index)
{
	if(string[index]=='t')
	{
		this->value = true;
		index+=4;
	}
	else
	{
		this->value = false;
		index+=5;
	}
	return this;
}

void _Bool::print(int level)
{
	std::string tab(level,'\t');
	printf("\n%s%s",tab.c_str(),toString().c_str());	
}


std::string _Bool::toString()
{
	std::string result;
    value?result="true":result="false";
    return result;
}
