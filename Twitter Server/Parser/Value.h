#pragma once
#include <string>
#include <ostream>

enum ValueType
{
	_bool,
	_mass,
	_null,
	_number,
	_object,
	_string
};

class Value 
{
public:
	ValueType type;

	virtual Value* parse(std::string & string, int & index)=0;

	virtual void print(int level=0)=0;
};

