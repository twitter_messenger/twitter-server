#include "_Null.h"


_Null::_Null(void)
{
	type=_null;
}


_Null::~_Null(void)
{
}

void _Null::print(int level)
{
	std::string tab(level,'\t');
	printf("\n%s%s",tab.c_str(),toString().c_str());	
}

_Null* _Null::parse(std::string & string,int & index)
{
	index+=4;
	return this;
}

std::string _Null::toString()
{
	return std::string("null");
}