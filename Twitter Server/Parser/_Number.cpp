#include "_Number.h"


_Number::_Number(void)
{
	type=_number;
}


_Number::~_Number(void)
{
}

void _Number::print(int level)
{
	std::string tab(level,'\t');
	printf("\n%s%s",tab.c_str(),toString().c_str());	
}

_Number* _Number::parse(std::string & string,int & index)
{
	std::string value="";
	while(string[index]!=','&& string[index]!='}' && string[index]!=']')
	{
		if (string[index]=='.')
			value+=',';
		else
			value+=string[index];
		index++;
	}

	this->value =atof(value.c_str());
	return this;
}

std::string _Number::toString()
{
	char buf[30];		
	if (isInt())
	{
		int tmp=(int)value;
		return std::string(itoa(tmp,buf,10));
	}

	sprintf(buf, "%f",value);

	return std::string(buf);
}

bool _Number::isInt()
{
	return value-((int)value)<0.0001;
}
