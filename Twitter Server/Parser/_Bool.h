#pragma once
#include "Value.h"
class _Bool : public Value
{
public:
	_Bool(void);
	~_Bool(void);
	bool value;
	_Bool* parse(std::string & string,int & index);
	std::string toString();
	void print(int level=0);
};

