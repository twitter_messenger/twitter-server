#include "_Mass.h"

_Mass::_Mass(void)
{
	type=_mass;
}


_Mass::~_Mass(void)
{
}

void _Mass::print(int level)
{
	std::string tab(level,'\t');

	printf("\n%s[",tab.c_str());

	for (auto i=value.cbegin();i!=value.cend();i++)
	{
		printf("\n%s",tab.c_str());		
		(*i)->print(level+1);
		//printf("\n%s,");
	}

	printf("\n%s]",tab.c_str());
}

_Mass* _Mass::parse(std::string & string,int & index)
{
	index++;
	while(isspace(string[index]))
	{
		index++;
	}
	do
	{
		if(string[index]=='{')
		{
			_Object* obj=new _Object();
			obj->parse(string,index);
			this->value.push_back(obj);
		}
		else if(string[index]=='t' || string[index]=='f')
		{
			_Bool* Bool=new _Bool();
			Bool->parse(string,index);
			this->value.push_back(Bool);
		}
		else if(string[index]=='n')
		{
			_Null* Null=new _Null();
			Null->parse(string,index);
			this->value.push_back(Null);
		}
		else if(string[index]=='"')
		{
			_String* str=new _String();
			str->parse(string,index);
			this->value.push_back(str);			
		}
		else if(string[index]=='[')
		{
			_Mass* mass=new _Mass();
			mass->parse(string,index);
			this->value.push_back(mass);
		}
		else if (isdigit(string[index]) || string[index]=='-')
		{
			_Number* num=new _Number();
			num->parse(string,index);
			this->value.push_back(num);
		}
		while(isspace(string[index]) || string[index]==',')
		{
			index++;
		}
	}
	while(string[index]!=']');
	index++;
	/*while(isspace(string[index]))
	{
		index++;
	}*/
	return this;
}
