#include "Request.h"


Request::Request()
{
	manager= new QNetworkAccessManager(this);	
	loop = new QEventLoop();

	connect(manager,SIGNAL(finished(QNetworkReply*)), loop, SLOT(quit()));
	connect(manager,SIGNAL(finished(QNetworkReply*)),this,SLOT(getResponse(QNetworkReply*)));

	multipart=NULL;
}


Request::~Request()
{
	manager->deleteLater();
	loop->deleteLater();
}

void Request::setURL(QByteArray url)
{
	request.setUrl(QUrl(url));
}
	
void Request::setHeader(QByteArray headerName,QByteArray value)
{
	request.setRawHeader(headerName,value);
}	

QByteArray Request::post()
{	
	request.setRawHeader("Content-Type","application/x-www-form-urlencoded");

	QNetworkReply * reply;
	if (multipart==NULL)
	{
		reply=manager->post(request,requestString);		
	}
	else
	{
		reply=manager->post(request,multipart);		
	}

	reply->setParent(manager);

	loop->exec();
	return result;
}


void Request::getResponse(QNetworkReply* reply)
{
	result=reply->readAll();
}

QByteArray Request::get()
{
	QNetworkReply * reply=manager->get(request);
	reply->setParent(manager);
	loop->exec();
	return result;
}

void Request::setHttpPart(QHttpPart * httpPart)
{
	if (multipart==NULL)
	{
		multipart=new QHttpMultiPart(this);
		multipart->setParent(manager);
	}

	multipart->append(*httpPart);
}