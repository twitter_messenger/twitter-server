#pragma once
#include "Request.h"
#include <qlist.h>
#include <QCryptographicHash>
#include <qdatetime.h>
#include <qregexp.h>
#include <qfile.h>
#include <qtextstream.h>
#include <Crypter.h>

/*
	�������� �����������:
	1. ��������� getRequestToken
	2. �������� url �������� getUrl
	3. ����� ������������� �� ����� ������ ���������� ��� � �������  setOAuthVerifier
	4. ��������� getAccessToken
	5. ������������ ���������� OAUTH_TOKEN � OAUTH_TOKEN_SECRET
*/
class OAuth
{
public:
	OAuth(void);
	~OAuth(void);

	static void getRequestToken();						// �������� request token
	static QByteArray getUrl();							// �������� ������ ��� �����������
	static void getAccessToken();						// �������� access token 
	static void setOAuthVerifier(QByteArray verifier);	// ������ oauth verifier (��� ����� �����������)

	static bool loadTokens();
	static bool saveTokens();
	static bool isValidTokens();
	static void clearTokens();

	static void signRequest(QList<QByteArray> & reqParams, Request & request, QByteArray url, QByteArray reqType="POST",bool addReqParamsToHeader=true);



	/*QByteArray getUsersData();
	QByteArray getFriendList();

	QByteArray doDirectMessage(QByteArray id,QByteArray text);
	QByteArray doTweetMessage(QByteArray text);

	QByteArray getLastMessages(int count=50);*/

	// ���������� ���������
	static QByteArray OAUTH_TOKEN;
	static QByteArray OAUTH_TOKEN_SECRET;
	static QByteArray USER_ID;
	static QByteArray SCREEN_NAME;
//private:
	//������������ ���������
	static const QByteArray CONSUMER_KEY;
	static const QByteArray CONSUMER_SECRET;
	static const QByteArray REQUEST_TOKEN_URL;
	static const QByteArray CALLBACK_URL;
	static const QByteArray ACCESS_TOKEN_URL;
	static const QByteArray AUTORIZE_URL;
	static const QByteArray ACCOUNT_DATA_URL;
	static const QByteArray URL_SEPARATOR;
	static const QByteArray OAUTH_SIGNATURE_METHOD;
	static const QByteArray OAUTH_VERSION;

	// ���������� ���������	
	static QByteArray OAUTH_VERIFIER;

	static QByteArray generateOAuthSignature(QList<QByteArray> & params, QByteArray url, QByteArray reqType="POST");
	static QByteArray generateUniqueString(int length=32);
	static QByteArray currentTime();
	static QByteArray hmacSha1(QByteArray key, QByteArray baseString);
 };

