#include "OAuth.h"

const QByteArray OAuth::CONSUMER_KEY="T394KplNqWyep41SNBERrtSPC";
const QByteArray OAuth::CONSUMER_SECRET="r67zdPkHyZBHcEOV45SMPUHe4TJs9LOYOde0oN5MrdHz5v8Tjy";
const QByteArray OAuth::REQUEST_TOKEN_URL="https://api.twitter.com/oauth/request_token";
const QByteArray OAuth::ACCESS_TOKEN_URL="https://api.twitter.com/oauth/access_token";
const QByteArray OAuth::CALLBACK_URL="oob";
const QByteArray OAuth::ACCOUNT_DATA_URL="https://api.twitter.com/1.1/users/show.json";
const QByteArray OAuth::URL_SEPARATOR="&";
const QByteArray OAuth::OAUTH_SIGNATURE_METHOD="HMAC-SHA1";
const QByteArray OAuth::OAUTH_VERSION="1.0";
const QByteArray OAuth::AUTORIZE_URL="https://api.twitter.com/oauth/authenticate";

QByteArray OAuth::OAUTH_TOKEN="";
QByteArray OAuth::OAUTH_TOKEN_SECRET="";
QByteArray OAuth::OAUTH_VERIFIER="";
QByteArray OAuth::USER_ID="";
QByteArray OAuth::SCREEN_NAME="";


OAuth::OAuth(void)
{
	loadTokens();
}

OAuth::~OAuth(void)
{	
	saveTokens();
}

void OAuth::getRequestToken()
{
	clearTokens();

	QList<QByteArray> params;
	params<<"oauth_callback=" + CALLBACK_URL;
	Request r;
	r.setURL(REQUEST_TOKEN_URL);
	signRequest(params,r,REQUEST_TOKEN_URL);

	QByteArray response=r.post();

	//���������� �����
	QRegExp parser("oauth_token=(.+)&oauth_token_secret=(.+)&");

	if (parser.indexIn(response)>-1)
	{
		OAUTH_TOKEN=parser.cap(1).toLocal8Bit();
		OAUTH_TOKEN_SECRET=parser.cap(2).toLocal8Bit();
	}
	else
	{
		////TODO ���������� ������
	}
}

void OAuth::setOAuthVerifier(QByteArray verifier)
{
	OAUTH_VERIFIER=verifier;
}
	
QByteArray OAuth::getUrl()
{
	return AUTORIZE_URL + "?oauth_token=" + OAUTH_TOKEN;
}

void OAuth::signRequest(QList<QByteArray> & reqParams, Request & request, QByteArray url, QByteArray reqType,bool addReqParamsToHeader)
{
	/// ������������� ���������� ���������
	QByteArray OAUTH_NONCE=generateUniqueString();
	QByteArray OAUTH_TIMESTAMP=currentTime();

	//����������� ��������� ��� ������������� oauth_signature
	QList<QByteArray> params;

	params<<reqParams;
	//params<<"oauth_callback=" + CALLBACK_URL;
	params<<"oauth_consumer_key=" + CONSUMER_KEY;
	params<<"oauth_nonce=" + OAUTH_NONCE ;
	params<<"oauth_signature_method="+ OAUTH_SIGNATURE_METHOD;
	params<<"oauth_timestamp=" + OAUTH_TIMESTAMP;
	if (OAUTH_TOKEN.length()>0)
		params<<"oauth_token=" + OAUTH_TOKEN;
	params<<"oauth_version=" + OAUTH_VERSION;

	// ������������� oauth_signature
	QByteArray OAUTH_SIGNATURE=generateOAuthSignature(params,url,reqType);

	// ����������� ���� �������
	QByteArray auth="OAuth ";	
	auth+="oauth_nonce=" + OAUTH_NONCE + ",";
	//auth+="oauth_callback="+ CALLBACK_URL + ",";
	auth+="oauth_signature_method=" + OAUTH_SIGNATURE_METHOD + ",";
	if (OAUTH_TOKEN.length()>0)
		auth+="oauth_token="+ OAUTH_TOKEN + ",";
	auth+="oauth_timestamp="+ OAUTH_TIMESTAMP + ",";
	auth+="oauth_consumer_key="+ CONSUMER_KEY + ",";	
	auth+="oauth_signature="+ QUrl::toPercentEncoding(OAUTH_SIGNATURE) + ",";
	for(int i=0;i<reqParams.count() && addReqParamsToHeader;i++)
		auth+=reqParams[i] + ",";
	auth+="oauth_version=1.0";

	request.setHeader("Authorization", auth);
}

void OAuth::getAccessToken()
{
	//����������� ��������� ��� ������������� oauth_signature
	QList<QByteArray> params;	
	params<<"oauth_verifier=" + OAUTH_VERIFIER;

	/// ��������� ������ � �������� �����
	Request r;
	r.setURL(ACCESS_TOKEN_URL);
	signRequest(params,r,ACCESS_TOKEN_URL);
	
	QByteArray response=r.post();

	//���������� �����
	QRegExp parser("oauth_token=(.+)&oauth_token_secret=([a-zA-Z0-9]+)&user_id=(\\d+)&screen_name=([a-zA-Z0-9]+)&");

	if (parser.indexIn(response)>-1)
	{
		OAUTH_TOKEN=parser.cap(1).toLocal8Bit();
		OAUTH_TOKEN_SECRET=parser.cap(2).toLocal8Bit();
		USER_ID=parser.cap(3).toLocal8Bit();
		SCREEN_NAME=parser.cap(4).toLocal8Bit();
		saveTokens();
	}
	else
	{
		////TODO ���������� ������
	}
}

bool OAuth::loadTokens()
{
	QFile file("Tokens.txt");

	file.open(QIODevice::ReadOnly);

	QTextStream in(&file);

	QString result=in.readAll();

	result=Crypter::decryptString(CONSUMER_KEY,result);

	QRegExp r("OAUTH_TOKEN=(.+)OAUTH_TOKEN_SECRET=(.+)USER_ID=(.+)SCREEN_NAME=(.+)");

	if (r.indexIn(result)>-1)
	{		
		OAUTH_TOKEN=r.cap(1).toLocal8Bit();
		OAUTH_TOKEN_SECRET=r.cap(2).toLocal8Bit();
		USER_ID=r.cap(3).toLocal8Bit();
		SCREEN_NAME=r.cap(4).toLocal8Bit();
		return isValidTokens();
	}
	else
	{
		clearTokens();
		return false;
	}

	return false;
}

bool OAuth::saveTokens()
{
	QFile file("Tokens.txt");

	file.open(QIODevice::WriteOnly);

	QTextStream out(&file);

	QString result;
	result+="OAUTH_TOKEN="+OAUTH_TOKEN;
	result+="OAUTH_TOKEN_SECRET="+OAUTH_TOKEN_SECRET;
	result+="USER_ID="+USER_ID;
	result+="SCREEN_NAME="+SCREEN_NAME;

	result=Crypter::cryptString(CONSUMER_KEY,result);

	out<<result;

	file.close();

	return true;
}

bool OAuth::isValidTokens()
{
	QRegExp validToken("\\d+-[a-z0-9A-Z]+");
	QRegExp validSecret("[a-z0-9A-Z]+");

	return validToken.exactMatch(OAUTH_TOKEN) && validSecret.exactMatch(OAUTH_TOKEN_SECRET);
}

void OAuth::clearTokens()
{
	OAUTH_VERIFIER="";
	OAUTH_TOKEN="";
	OAUTH_TOKEN_SECRET="";
	USER_ID="";
	SCREEN_NAME="";
}

//
//QByteArray OAuth::getUsersData()
//{
//	/// ������������� ���������� ���������
//	QByteArray OAUTH_NONCE=generateUniqueString();
//	QByteArray OAUTH_TIMESTAMP=currentTime();
//
//	//����������� ��������� ��� ������������� oauth_signature
//	QList<QByteArray> params;	
//	params<<"oauth_consumer_key=" + CONSUMER_KEY + URL_SEPARATOR;
//	params<<"oauth_token=" + OAUTH_TOKEN + URL_SEPARATOR;
//	params<<"oauth_nonce=" + OAUTH_NONCE + URL_SEPARATOR;
//	params<<"oauth_timestamp=" + OAUTH_TIMESTAMP + URL_SEPARATOR;
//	params<<"oauth_signature_method=" + OAUTH_SIGNATURE_METHOD + URL_SEPARATOR;
//	params<<"oauth_version=" + OAUTH_VERSION + URL_SEPARATOR;
//	params<<"user_id=" + USER_ID;
//
//
//	// ������������� oauth_signature
//	QByteArray OAUTH_SIGNATURE=generateOAuthSignature(params,ACCOUNT_DATA_URL,false);
//
//	// ����������� ���� �������
//	QByteArray auth="OAuth ";
//	auth+="oauth_nonce=\""+ OAUTH_NONCE + "\",";
//	auth+="oauth_signature_method=\"" + OAUTH_SIGNATURE_METHOD + "\",";
//	auth+="oauth_token=\""+ OAUTH_TOKEN + "\",";
//	auth+="oauth_timestamp=\""+ OAUTH_TIMESTAMP + "\",";
//	auth+="oauth_consumer_key=\""+ CONSUMER_KEY + "\",";
//	auth+="oauth_signature=\""+ QUrl::toPercentEncoding(OAUTH_SIGNATURE) + "\",";
//	auth+="oauth_version=\"1.0\"";
//
//	/// ��������� ������ � �������� �����
//	Request r;
//	r.setURL(ACCOUNT_DATA_URL + "?user_id=" + USER_ID );
//	r.setHeader("Authorization", auth);
//	QByteArray response=r.get();
//
//	return response;
//}
//
//QByteArray OAuth::getFriendList()
//{
//	QByteArray FRIEND_LIST_URL="https://api.twitter.com/1.1/friends/list.json";
//
//	/// ������������� ���������� ���������
//	QByteArray OAUTH_NONCE=generateUniqueString();
//	QByteArray OAUTH_TIMESTAMP=currentTime();
//
//	//����������� ��������� ��� ������������� oauth_signature
//	QList<QByteArray> params;	
//	params<<"oauth_consumer_key=" + CONSUMER_KEY + URL_SEPARATOR;
//	params<<"oauth_token=" + OAUTH_TOKEN + URL_SEPARATOR;
//	params<<"oauth_nonce=" + OAUTH_NONCE + URL_SEPARATOR;
//	params<<"oauth_timestamp=" + OAUTH_TIMESTAMP + URL_SEPARATOR;
//	params<<"oauth_signature_method=" + OAUTH_SIGNATURE_METHOD + URL_SEPARATOR;
//	params<<"oauth_version=" + OAUTH_VERSION + URL_SEPARATOR;
//	params<<"user_id=" + USER_ID;
//
//
//	// ������������� oauth_signature
//	QByteArray OAUTH_SIGNATURE=generateOAuthSignature(params,FRIEND_LIST_URL,false);
//
//	// ����������� ���� �������
//	QByteArray auth="OAuth ";
//	auth+="oauth_nonce=\""+ OAUTH_NONCE + "\",";
//	auth+="oauth_signature_method=\"" + OAUTH_SIGNATURE_METHOD + "\",";
//	auth+="oauth_token=\""+ OAUTH_TOKEN + "\",";
//	auth+="oauth_timestamp=\""+ OAUTH_TIMESTAMP + "\",";
//	auth+="oauth_consumer_key=\""+ CONSUMER_KEY + "\",";
//	auth+="oauth_signature=\""+ QUrl::toPercentEncoding(OAUTH_SIGNATURE) + "\",";
//	auth+="oauth_version=\"1.0\"";
//
//	/// ��������� ������ � �������� �����
//	Request r;
//	r.setURL(FRIEND_LIST_URL + "?user_id=" + USER_ID );
//	r.setHeader("Authorization", auth);
//	QByteArray response=r.get();
//
//	return response;
//}
//
//QByteArray OAuth::doDirectMessage(QByteArray id,QByteArray text)
//{
//	QString txt(text);
//
//	QByteArray NEW_DIRECT_MESSAGE_URL="https://api.twitter.com/1.1/direct_messages/new.json";
//
//	/// ������������� ���������� ���������
//	QByteArray OAUTH_NONCE=generateUniqueString();
//	QByteArray OAUTH_TIMESTAMP=currentTime();
//
//	//����������� ��������� ��� ������������� oauth_signature
//	QList<QByteArray> params;	
//	params<<"oauth_consumer_key=" + CONSUMER_KEY + URL_SEPARATOR;
//	params<<"oauth_token=" + OAUTH_TOKEN + URL_SEPARATOR;
//	params<<"oauth_nonce=" + OAUTH_NONCE + URL_SEPARATOR;
//	params<<"oauth_timestamp=" + OAUTH_TIMESTAMP + URL_SEPARATOR;
//	params<<"oauth_signature_method=" + OAUTH_SIGNATURE_METHOD + URL_SEPARATOR;
//	params<<"oauth_version=" + OAUTH_VERSION + URL_SEPARATOR;
//	params<<"text=" + QUrl::toPercentEncoding(text) + URL_SEPARATOR;
//	params<<"user_id=" + id;
//
//
//	// ������������� oauth_signature
//	QByteArray OAUTH_SIGNATURE=generateOAuthSignature(params,NEW_DIRECT_MESSAGE_URL,true);
//
//	// ����������� ���� �������
//	QByteArray auth="OAuth ";
//	auth+="oauth_nonce=\""+ OAUTH_NONCE + "\",";
//	auth+="oauth_signature_method=\"" + OAUTH_SIGNATURE_METHOD + "\",";
//	auth+="oauth_token=\""+ QUrl::toPercentEncoding(OAUTH_TOKEN) + "\",";
//	auth+="oauth_timestamp=\""+ OAUTH_TIMESTAMP + "\",";
//	auth+="oauth_consumer_key=\""+ CONSUMER_KEY + "\",";
//	auth+="oauth_signature=\""+ QUrl::toPercentEncoding(OAUTH_SIGNATURE) + "\",";
//	auth+="oauth_version=\"1.0\"";
//
//	/// ��������� ������ � �������� �����
//	Request r;
//	r.setURL(NEW_DIRECT_MESSAGE_URL);
//	r.requestString="text=" + QUrl::toPercentEncoding(text) + "&user_id=" + QUrl::toPercentEncoding(id);
//	r.setHeader("Authorization", auth);
//	QByteArray response=r.post();
//
//	return response;
//}
//
//QByteArray OAuth::doTweetMessage(QByteArray text)
//{
//	QString txt(text);
//
//	QByteArray NEW_TWEET_MESSAGE_URL="https://api.twitter.com/1.1/statuses/update.json";
//
//	/// ������������� ���������� ���������
//	QByteArray OAUTH_NONCE=generateUniqueString();
//	QByteArray OAUTH_TIMESTAMP=currentTime();
//
//	//����������� ��������� ��� ������������� oauth_signature
//	QList<QByteArray> params;	
//	params<<"oauth_consumer_key=" + CONSUMER_KEY + URL_SEPARATOR;
//	params<<"oauth_token=" + OAUTH_TOKEN + URL_SEPARATOR;
//	params<<"oauth_nonce=" + OAUTH_NONCE + URL_SEPARATOR;
//	params<<"oauth_timestamp=" + OAUTH_TIMESTAMP + URL_SEPARATOR;
//	params<<"oauth_signature_method=" + OAUTH_SIGNATURE_METHOD + URL_SEPARATOR;
//	params<<"oauth_version=" + OAUTH_VERSION + URL_SEPARATOR;
//	params<<"status=" + QUrl::toPercentEncoding(text);
//	//params<<"user_id=" + id;
//
//
//	// ������������� oauth_signature
//	QByteArray OAUTH_SIGNATURE=generateOAuthSignature(params,NEW_TWEET_MESSAGE_URL,true);
//
//	// ����������� ���� �������
//	QByteArray auth="OAuth ";
//	auth+="oauth_nonce=\""+ OAUTH_NONCE + "\",";
//	auth+="oauth_signature_method=\"" + OAUTH_SIGNATURE_METHOD + "\",";
//	auth+="oauth_token=\""+ QUrl::toPercentEncoding(OAUTH_TOKEN) + "\",";
//	auth+="oauth_timestamp=\""+ OAUTH_TIMESTAMP + "\",";
//	auth+="oauth_consumer_key=\""+ CONSUMER_KEY + "\",";
//	auth+="oauth_signature=\""+ QUrl::toPercentEncoding(OAUTH_SIGNATURE) + "\",";
//	auth+="oauth_version=\"1.0\"";
//
//	/// ��������� ������ � �������� �����
//	Request r;
//	r.setURL(NEW_TWEET_MESSAGE_URL);
//	r.requestString="status=" + QUrl::toPercentEncoding(text) /*+ "&user_id=" + QUrl::toPercentEncoding(id)*/;
//	r.setHeader("Authorization", auth);
//	QByteArray response=r.post();
//
//	return response;
//}
//
//QByteArray OAuth::getLastMessages(int count)
//{
//	QByteArray GET_DIRECT_MESSAGES_URL="https://api.twitter.com/1.1/direct_messages.json";
//
//	/// ������������� ���������� ���������
//	QByteArray OAUTH_NONCE=generateUniqueString();
//	QByteArray OAUTH_TIMESTAMP=currentTime();
//
//	//����������� ��������� ��� ������������� oauth_signature
//	QList<QByteArray> params;	
//	params<<"oauth_consumer_key=" + CONSUMER_KEY + URL_SEPARATOR;
//	params<<"oauth_token=" + OAUTH_TOKEN + URL_SEPARATOR;
//	params<<"oauth_nonce=" + OAUTH_NONCE + URL_SEPARATOR;
//	params<<"oauth_timestamp=" + OAUTH_TIMESTAMP + URL_SEPARATOR;
//	params<<"oauth_signature_method=" + OAUTH_SIGNATURE_METHOD + URL_SEPARATOR;
//	params<<"oauth_version=" + OAUTH_VERSION + URL_SEPARATOR;
//	params<<"count=" + QByteArray::number(count) + URL_SEPARATOR;
//	params<<"include_entities=false" + URL_SEPARATOR;
//	params<<"skip_status=true";
//
//	// ������������� oauth_signature
//	QByteArray OAUTH_SIGNATURE=generateOAuthSignature(params,GET_DIRECT_MESSAGES_URL,false);
//
//	// ����������� ���� �������
//	QByteArray auth="OAuth ";
//	auth+="oauth_nonce=\""+ OAUTH_NONCE + "\",";
//	auth+="oauth_signature_method=\"" + OAUTH_SIGNATURE_METHOD + "\",";
//	auth+="oauth_token=\""+ OAUTH_TOKEN + "\",";
//	auth+="oauth_timestamp=\""+ OAUTH_TIMESTAMP + "\",";
//	auth+="oauth_consumer_key=\""+ CONSUMER_KEY + "\",";
//	auth+="oauth_signature=\""+ QUrl::toPercentEncoding(OAUTH_SIGNATURE) + "\",";
//	auth+="oauth_version=\"1.0\"";
//
//
//	/// ��������� ������ � �������� �����
//	Request r;
//	r.setURL(GET_DIRECT_MESSAGES_URL + "?count=" + QByteArray::number(count) +  "&include_entities=false&skip_status=true");
//	r.setHeader("Authorization", auth);
//	QByteArray response=r.get();
//
//	return response;
//}



QByteArray OAuth::generateOAuthSignature(QList<QByteArray> & params, QByteArray url, QByteArray reqType)
{
	/// ������������� �� ��������
	qSort(params);

	/// ������������� ������� ������
	QByteArray base;
	for (int i=0;i<params.length();i++)
	{
		base+=QUrl::toPercentEncoding(params[i]);

		if (i!=params.length()-1)
			base+=QUrl::toPercentEncoding(URL_SEPARATOR);
	}
	
	base.prepend(reqType+URL_SEPARATOR+QUrl::toPercentEncoding(url) + URL_SEPARATOR);
	
	// ������������� ����
	QByteArray key = CONSUMER_SECRET + URL_SEPARATOR + OAUTH_TOKEN_SECRET;

	return hmacSha1(key,base);
}
	
QByteArray OAuth::generateUniqueString(int length)
{
	QByteArray arr;
	char c;
	for (int i=0;i<length;i++)
	{
		c=(65 + rand() % 25);	
		
		arr.append(c);
	}
	return arr;
}
	
QByteArray OAuth::currentTime()
{
	return QByteArray::number(QDateTime(QDate(1970,1,1),QTime(0,0)).secsTo(QDateTime::currentDateTime()));
}
	
QByteArray OAuth::hmacSha1(QByteArray key, QByteArray baseString)
{
	int blockSize = 64; // HMAC-SHA-1 block size, defined in SHA-1 standard
	 if (key.length() > blockSize) { // if key is longer than block size (64), reduce key length with SHA-1 compression
	 key = QCryptographicHash::hash(key, QCryptographicHash::Sha1);
	 }
 
	QByteArray innerPadding(blockSize, char(0x36)); // initialize inner padding with char "6"
	 QByteArray outerPadding(blockSize, char(0x5c)); // initialize outer padding with char "quot;
	 // ascii characters 0x36 ("6") and 0x5c ("quot;) are selected because they have large
	 // Hamming distance (http://en.wikipedia.org/wiki/Hamming_distance)
 
	for (int i = 0; i < key.length(); i++) {
	 innerPadding[i] = innerPadding[i] ^ key.at(i); // XOR operation between every byte in key and innerpadding, of key length
	 outerPadding[i] = outerPadding[i] ^ key.at(i); // XOR operation between every byte in key and outerpadding, of key length
	 }
 
	// result = hash ( outerPadding CONCAT hash ( innerPadding CONCAT baseString ) ).toBase64
	 QByteArray total = outerPadding;
	 QByteArray part = innerPadding;
	 part.append(baseString);
	 total.append(QCryptographicHash::hash(part, QCryptographicHash::Sha1));
	 QByteArray hashed = QCryptographicHash::hash(total, QCryptographicHash::Sha1);
	 return hashed.toBase64();
}