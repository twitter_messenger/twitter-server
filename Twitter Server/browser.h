#ifndef BROWSER_H
#define BROWSER_H

#include <QWidget>
#include "GeneratedFiles\ui_browser.h"
#include <qnetworkaccessmanager.h>
#include <qeventloop.h>
#include <qregexp.h>
#include <qwebframe>
#include <qmessagebox.h>
#include <qtextcodec.h>

class Browser : public QWidget
{
	Q_OBJECT

public:
	Browser(QWidget *parent = 0);
	~Browser();

	QByteArray loadAndGetVerifier(QByteArray url);
private:
	QEventLoop * loop;
	QEventLoop * loop2;
	QEventLoop * loop3;
	Ui::Browser ui;
	QUrl newUrl;
	QTextCodec *tc/*=QTextCodec::codecForName("CP1251")*/;
private slots:
	void setNewUrl(const QUrl & url);

		
};

#endif // BROWSER_H
