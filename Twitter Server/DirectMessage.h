#pragma once
#include <qchar.h>
#include <qdatetime.h>


struct DateTime
{
	ushort sec;
	ushort min;
	ushort hour;
	ushort day;
	ushort month;
	ushort year;
};


class DirectMessage
{
public:
	DirectMessage()
	{
	}

	DirectMessage(QString message,qulonglong recipientID,qulonglong senderID=OAuth::USER_ID.toULongLong())
	{
		this->senderID=senderID;
		this->recipientID=recipientID;
		setMessage(message);
	}


	void setMessage(QString & mes)
	{
		for (int i=0;i<mes.length() && i<10000;i++)
		{
			this->message[i]=mes[i];
		}

		this->messageLength=mes.count();

		if (this->messageLength>10000)
			messageLength=10000;

		this->message[messageLength]='\0';
	}

	bool operator<(const DirectMessage & other) const
	{
		return QDateTime(QDate(date.year,date.month,date.day),QTime(date.hour,date.min,date.sec))<QDateTime(QDate(other.date.year,other.date.month,other.date.day),QTime(other.date.hour,other.date.min,other.date.sec));
	}

	DateTime date;
	unsigned long long messageID;
	unsigned long long senderID;
	unsigned long long recipientID;
	uint messageLength;
	QChar message[10001];
};