
#include <QtCore/QCoreApplication>
#include "OAuth.h"
#include "browser.h"
#include "TwitterRequests.h"

// LOCAL_DEBUG
#ifdef LOCAL_DEBUG

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);

	bool isOk;	
	//itterRequests::loadOldTokens();	
	TwitterRequests::loginNewUser();
	int count=0;

	//DirectMessage mes("message",789549970156318720);

	isOk=TwitterRequests::getStatus();


	/*QHash<qulonglong,QList<DirectMessage> > messages;
	QList <DirectMessage> mes;
	do
	{		
		mes=TwitterRequests::getLastMessages(0,200,&isOk);

		for (int i=0;i<mes.count();i++)
			messages[mes[i].senderID]<<mes[i];

		mes=TwitterRequests::getLastSentMessages(0,200,&isOk);

		for (int i=0;i<mes.count();i++)
			messages[mes[i].recipientID]<<mes[i];

		for (auto i=messages.begin();i!=messages.end();++i)
		{
			qSort(i.value());
		}

		printf("\n%d",count);
	} while (isOk);*/
	return a.exec();
}

#else

#include <Windows.h>

#define STOP 0									// ��������� �������
#define GET_AUTH_STATUS 1 						// ��������� ������ ����������� (��� ��������� ����������� �� ������, �������� ���������)
#define LOGIN_NEW_USER 2						// ����������� ������ ������������ (��� ���������  ����������� �� ������)
#define LOAD_OLD_TOKENS 3						// ����������� ������������ ������� ������ (��� ���������  ����������� �� ������)
#define GET_USER_INFO 4							// ��������� ���������� �� �������������� ������������ (���� ��������� ��������� �����������)
#define GET_FRIEND_IDS 5						// ��������� ������� id ������ (���� ��������� ��������� �����������, ���������������� ������������� GET_FRIENDS)	
#define GET_FRIENDS 6							// �������� ������ ������ (���� ��������� ��������� �����������)
#define SEND_MESSAGE 7							// �������� ��������� (���� �������������� ��������� �����������)
#define GET_RECEIVED_MESSAGES_AFTER 8			// �������� ��������� ��������� ������ �� ������ (����� ��������� ��� ���), ������� � �������������
#define GET_SENT_MESSAGES_AFTER 9				// �������� ��������� ������������ ���� ���������(��������� ������ ��� ���), ������� � �������������
#define GET_RECEIVED_MESSAGES_BEFORE 10			// �������� ��������� ������ �� ������ (����� ��������� ��� ���), �� ������������� ���������
#define GET_SENT_MESSAGES_BEFORE 11				// �������� ��������� ������������ ���� ���������(��������� ������ ��� ���), �� ������������� ���������
#define CLIENT_SYNC 12                          // ������������� � ��������		

DWORD WINAPI messageThread(LPVOID lpParam);

CRITICAL_SECTION crSec; 

int main(int argc, char *argv[])
{
	setlocale(LC_CTYPE, "rus");

	QApplication a(argc, argv);

	QTextCodec *tc=QTextCodec::codecForName("CP1251");
	QTextCodec::setCodecForCStrings(tc);

	// ���������������� ����������� ������
	InitializeCriticalSection(&crSec);

	// ��������� ������ ������
	bool tokensLoaded=TwitterRequests::loadOldTokens();

	HANDLE hPipe=CreateNamedPipeA(("\\\\.\\pipe\\twitter"),PIPE_ACCESS_DUPLEX,PIPE_TYPE_MESSAGE|PIPE_READMODE_MESSAGE|PIPE_WAIT,PIPE_UNLIMITED_INSTANCES,1024,1024,5000,NULL);
	if(hPipe==INVALID_HANDLE_VALUE)		
		return 0;	
	HANDLE mesPipe=CreateNamedPipeA(("\\\\.\\pipe\\twitter_messages"),PIPE_ACCESS_DUPLEX,PIPE_TYPE_MESSAGE|PIPE_READMODE_MESSAGE|PIPE_WAIT,PIPE_UNLIMITED_INSTANCES,1024,1024,5000,NULL);
	if(mesPipe==INVALID_HANDLE_VALUE)
		return 0;

	// ���������� �������	
	printf("\nPipes created. Waiting for client ...");
	ConnectNamedPipe(hPipe,NULL);
	ConnectNamedPipe(mesPipe,NULL);
	printf("\nClient connected");

	// ��������� ����� ������ �����������
	CreateThread(NULL,NULL,&messageThread,&mesPipe,NULL,NULL);	

	int req=-1;
	int count;
	int sync_id=28;
	qulonglong since_id;
	qulonglong max_id;
	bool isOk;
	DWORD read,write;
	Person person;
	DirectMessage message;
	QList<unsigned long long> ids;
	QList<Person> persons;
	QList<DirectMessage> messages;
	
	do
	{
		ReadFile(hPipe,(void*)&req,sizeof(int),&read,NULL);

		EnterCriticalSection(&crSec);
		switch (req)
		{
			case GET_AUTH_STATUS:
				tokensLoaded=TwitterRequests::getStatus();
				printf("\nRequest=\"GET_AUTH_STATUS\", success=%s",tokensLoaded?"true":"false");
				WriteFile(hPipe,(void*)&tokensLoaded,sizeof(bool),&write,NULL);
				break;

			case LOGIN_NEW_USER:
				tokensLoaded=TwitterRequests::loginNewUser();
				printf("\nRequest=\"LOGIN_NEW_USER\", success=%s",tokensLoaded?"true":"false");
				WriteFile(hPipe,(void*)&tokensLoaded,sizeof(bool),&write,NULL);
				break;

			case LOAD_OLD_TOKENS:
				tokensLoaded=TwitterRequests::loadOldTokens();
				printf("\nRequest=\"LOAD_OLD_TOKENS\", success=%s",tokensLoaded?"true":"false");
				WriteFile(hPipe,(void*)&tokensLoaded,sizeof(bool),&write,NULL);
				break;

			case GET_USER_INFO:
				person=TwitterRequests::getUserInformation(&isOk);
				WriteFile(hPipe,(void*)&isOk,sizeof(bool),&write,NULL);
				if (isOk)
					WriteFile(hPipe,(void*)&person,sizeof(Person),&write,NULL);		
				printf("\nRequest=\"GET_USER_INFO\", success=%s",isOk?"true":"false");
				break;

			case GET_FRIEND_IDS:
				ids=TwitterRequests::getFrindIDs(&isOk);
				WriteFile(hPipe,(void*)&isOk,sizeof(bool),&write,NULL);
				if (isOk)
				{
					count=ids.count();
					WriteFile(hPipe,(void*)&count,sizeof(int),&write,NULL);
					for (int i=0;i<count;i++)
						WriteFile(hPipe,(void*)&(ids[i]),sizeof(ULONGLONG),&write,NULL);
				}
				printf("\nRequest=\"GET_FRIEND_IDS\", success=%s",isOk?"true":"false");
				break;

			case GET_FRIENDS:
				persons=TwitterRequests::getFrinds(&isOk);
				WriteFile(hPipe,(void*)&isOk,sizeof(bool),&write,NULL);
				if (isOk)
				{
					count=persons.count();
					WriteFile(hPipe,(void*)&count,sizeof(int),&write,NULL);
					for (int i=0;i<count;i++)
						WriteFile(hPipe,(void*)&(persons[i]),sizeof(Person),&write,NULL);
				}
				printf("\nRequest=\"GET_FRIENDS\", success=%s",isOk?"true":"false");
				break;

			

			case GET_RECEIVED_MESSAGES_AFTER:
				ReadFile(hPipe,(void*)&since_id,sizeof(qulonglong),&read,NULL);
				ReadFile(hPipe,(void*)&count,sizeof(int),&read,NULL);
				messages=TwitterRequests::getLastMessages(since_id,count,&isOk);
				WriteFile(hPipe,(void*)&isOk,sizeof(bool),&write,NULL);
				if (isOk)
				{
					count=messages.count();
					WriteFile(hPipe,(void*)&count,sizeof(int),&write,NULL);
					for (int i=0;i<messages.count();i++)
						WriteFile(hPipe,(void*)&(messages[i]),sizeof(DirectMessage),&write,NULL);
				}

				break;

			case GET_RECEIVED_MESSAGES_BEFORE:
				ReadFile(hPipe,(void*)&max_id,sizeof(qulonglong),&read,NULL);
				ReadFile(hPipe,(void*)&count,sizeof(int),&read,NULL);
				messages=TwitterRequests::getMessagesBefore(max_id,count,&isOk);
				WriteFile(hPipe,(void*)&isOk,sizeof(bool),&write,NULL);
				if (isOk)
				{
					count=messages.count();
					WriteFile(hPipe,(void*)&count,sizeof(int),&write,NULL);
					for (int i=0;i<messages.count();i++)
						WriteFile(hPipe,(void*)&(messages[i]),sizeof(DirectMessage),&write,NULL);
				}
				break;

			case GET_SENT_MESSAGES_AFTER:
				ReadFile(hPipe,(void*)&since_id,sizeof(qulonglong),&read,NULL);
				ReadFile(hPipe,(void*)&count,sizeof(int),&read,NULL);
				messages=TwitterRequests::getLastSentMessages(since_id,count,&isOk);
				WriteFile(hPipe,(void*)&isOk,sizeof(bool),&write,NULL);
				if (isOk)
				{
					count=messages.count();
					WriteFile(hPipe,(void*)&count,sizeof(int),&write,NULL);
					for (int i=0;i<messages.count();i++)
						WriteFile(hPipe,(void*)&(messages[i]),sizeof(DirectMessage),&write,NULL);
				}

				break;

			case GET_SENT_MESSAGES_BEFORE:
				ReadFile(hPipe,(void*)&max_id,sizeof(qulonglong),&read,NULL);
				ReadFile(hPipe,(void*)&count,sizeof(int),&read,NULL);
				messages=TwitterRequests::getSentMessagesBefore(max_id,count,&isOk);
				WriteFile(hPipe,(void*)&isOk,sizeof(bool),&write,NULL);
				if (isOk)
				{
					count=messages.count();
					WriteFile(hPipe,(void*)&count,sizeof(int),&write,NULL);
					for (int i=0;i<messages.count();i++)
						WriteFile(hPipe,(void*)&(messages[i]),sizeof(DirectMessage),&write,NULL);
				}
				break;
			case CLIENT_SYNC:
				WriteFile(hPipe,(void*)&sync_id,sizeof(int),&write,NULL);
				break;

			case SEND_MESSAGE:
				ReadFile(hPipe,(void*)&message,sizeof(DirectMessage),&read,NULL);				
				isOk=TwitterRequests::sendMessage(message);
				WriteFile(hPipe,(void*)&isOk,sizeof(bool),&write,NULL);
				if (isOk)
				{
					WriteFile(hPipe,(void*)&message,sizeof(DirectMessage),&write,NULL);
				}
				break;
		}
		LeaveCriticalSection(&crSec);

	} while(req!=STOP);

	printf("\nFinish!");
    DisconnectNamedPipe(hPipe);
    CloseHandle(hPipe);	

	return a.exec();
}


/*!
	������������ ������ 2 ������� : GET_RECEIVED_MESSAGES_AFTER � STOP

	���������� ������������� GET_RECEIVED_MESSAGES_AFTER - ����� � GET_RECEIVED_MESSAGES_AFTER �������� id ������ ���������� ���������� ���������
 */
DWORD WINAPI messageThread(LPVOID lpParam)
{
	HANDLE hPipe=*(HANDLE*)lpParam;

	int req,count;
	DWORD read,write;
	QList<DirectMessage> messages;
	bool isOk;
	qulonglong since_id;

	do
	{
		ReadFile(hPipe,(void*)&req,sizeof(int),&read,NULL);

		EnterCriticalSection(&crSec);
		if (req==GET_RECEIVED_MESSAGES_AFTER)
		{
			ReadFile(hPipe,(void*)&since_id,sizeof(qulonglong),&read,NULL);
			ReadFile(hPipe,(void*)&count,sizeof(int),&read,NULL);
			messages=TwitterRequests::getLastMessages(since_id,count,&isOk);
			WriteFile(hPipe,(void*)&isOk,sizeof(bool),&write,NULL);
			if (isOk)
			{
				count=messages.count();
				WriteFile(hPipe,(void*)&count,sizeof(int),&write,NULL);
				for (int i=0;i<messages.count();i++)
					WriteFile(hPipe,(void*)&(messages[i]),sizeof(DirectMessage),&write,NULL);
			}
		}
		LeaveCriticalSection(&crSec);

	} while(req!=STOP);

	return 0;
}



#endif