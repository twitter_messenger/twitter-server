#include "browser.h"

Browser::Browser(QWidget *parent)
	: QWidget(parent)
{
	ui.setupUi(this);
	loop=new QEventLoop(this);
	loop2=new QEventLoop(this);
	//loop3=new QEventLoop(this);

	tc=QTextCodec::codecForName("CP1251");
	QTextCodec::setCodecForCStrings(tc);

	QObject::connect(ui.webView,SIGNAL(loadFinished(bool)), loop, SLOT(quit()));
	QObject::connect(ui.webView,SIGNAL(urlChanged(const QUrl &)), loop2, SLOT(quit()));
	QObject::connect(ui.webView,SIGNAL(urlChanged(const QUrl &)), this, SLOT(setNewUrl(const QUrl &)));

}

Browser::~Browser()
{
	loop->deleteLater();
	loop2->deleteLater();
}

void Browser::setNewUrl(const QUrl & url)
{
	newUrl=url;
}

QByteArray Browser::loadAndGetVerifier(QByteArray url)
{
	QRegExp code("<code>(\\d{7})</code>");

	ui.webView->load(QUrl(url));
	
	loop->exec();  // ������� ���������	
	
	loop2->exec(); // ��������� �������� ����� ��������
		
	loop->exec();
	if (newUrl==QUrl("https://api.twitter.com/oauth/authenticate"))
	{
		QString s=(ui.webView->page()->currentFrame()->toHtml());
		if (code.indexIn(s)>-1)
		{
			return code.cap(1).toLocal8Bit();
		}
		else
		{
				 
		}
	}

	QWebPage * page=new QWebPage(this);
	ui.webView->setPage(page);
	page->deleteLater();

	QMessageBox msgBox;  
	msgBox.setText("������ �����!"); 
	msgBox.setInformativeText("������ ����������� �����?"); 
	msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No); 
	msgBox.setIcon(QMessageBox::Warning); 
	msgBox.setDefaultButton(QMessageBox::Yes); 
	int res = msgBox.exec(); 
	if (QMessageBox::Yes) //������ ������ Ok        
		return "try"; 
	else //������         
		return "stop";  

	return "";
}