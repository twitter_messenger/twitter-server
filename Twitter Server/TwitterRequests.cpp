#include "TwitterRequests.h"

TwitterRequests::TwitterRequests(QObject *parent)
	: QObject(parent)
{

}

TwitterRequests::~TwitterRequests()
{

}


bool TwitterRequests::loginNewUser()
{
	OAuth::clearTokens();

	QByteArray response;
	Browser b;
	b.show();
	do
	{
		OAuth::getRequestToken();
		response=b.loadAndGetVerifier(OAuth::getUrl());

	} while (response=="try");

	b.close();

	if (response=="stop")
		return false;

	OAuth::setOAuthVerifier(response);

	OAuth::getAccessToken();

	return OAuth::isValidTokens();
}


bool TwitterRequests::loadOldTokens()
{
	OAuth::loadTokens();

	if (OAuth::isValidTokens())
		return true;
	else	
		OAuth::clearTokens();
		
	
	return false;
}

Person TwitterRequests::getUserInformation(bool * isOk,unsigned long long id)
{
	Person person;

	if (isOk!=NULL)
		*isOk=true;

	QByteArray ACCOUNT_DATA_URL="https://api.twitter.com/1.1/users/show.json";

	// ��������� ������
	Request r;
	QList<QByteArray> params;
	params<<"user_id=" + QByteArray::number(id);
	r.setURL(ACCOUNT_DATA_URL+ "?" +params[0]);
	OAuth::signRequest(params,r,ACCOUNT_DATA_URL,"GET");
	QByteArray response=r.get();

	// ���������� �����
	std::string json(response.data());

	_Object obj;
	int index=1;
	obj.parse(json,index);

	// ��������� ����� �� ������
	if ( isOk!=NULL && (obj.contains("errors") || !obj.contains("name") || !obj.contains("profile_image_url")))
	{
		*isOk=false;
		return person;
	}
	
	
	decodeFromUtf16(((_String *)obj.getValue("name"))->value,person.name);	
	person.id=id;
	decodeFromUtf16(QString(((_String *)obj.getValue("profile_image_url"))->value).replace("\\/","/"),person.imgUrl);

	return person;
}

void TwitterRequests::decodeFromUtf16(QString input, QChar * result)
{
	QRegExp isUnicode("([^\\\\]|^)\\\\u[a-f0-9A-F]{4}");
	QRegExp toUnicode("([^\\\\]|^)([a-f0-9A-F]{4}|[^\\\\u0-9A-Fa-f])");
	QChar c;

	if (isUnicode.indexIn(input)>-1)
	{
		int pos=0;
		int i=0;
		QString s;

		while ((pos=toUnicode.indexIn(input,pos))>-1)
		{
			s=toUnicode.cap(2);
			
			if (s.length()==1)
				result[i]=s[0];
			else
				result[i]=s.toUShort(NULL,16);

			pos+=s.length();

			i++;
		}

		result[i]='\0';
	}
	else
	{
		for (int i=0;i<input.size();i++)
		{
			result[i]=input[i];
		}
		result[input.count()]='\0';
	}
}

QList<unsigned long long> TwitterRequests::getFrindIDs(bool * isOk,unsigned long long id)
{
	QList<unsigned long long> list;

	if (isOk!=NULL)
		*isOk=true;

	QByteArray URL="https://api.twitter.com/1.1/friends/ids.json";
	QByteArray cursor="-1";

	// ��������� ������
	do
	{
		Request r;
		QList<QByteArray> params;
		params<<"user_id=" + QByteArray::number(id);
		params<<"cursor=" + cursor;
		r.setURL(URL+ "?" +params[0] + "&" + params[1]);
		OAuth::signRequest(params,r,URL,"GET");
		QByteArray response=r.get();

		// ���������� �����
		std::string json(response.data());
		_Object obj;
		int index=1;
		obj.parse(json,index);

		// ��������� ����� �� ������
		if ( isOk!=NULL && (obj.contains("errors") || !obj.contains("next_cursor_str") || !obj.contains("ids")))
		{
			*isOk=false;
			return list;
		}

		cursor=((_String *)obj.getValue("next_cursor_str"))->value;

		// ������� ids � ������
		auto & arr=((_Mass *)obj.getValue("ids"))->value;
		for (auto i=arr.cbegin();i!=arr.cend();++i)
		{
			list<<QString(((_String*)(*i))->value).toULongLong();
		}

	} while (cursor!="0");

	return list;
}

QList<Person> TwitterRequests::getFrinds(bool * isOk,unsigned long long id)
{
	QList<Person> list;

	if (isOk!=NULL)
		*isOk=true;

	QByteArray URL="https://api.twitter.com/1.1/friends/list.json";
	QByteArray cursor="-1";

	// ��������� ������
	do
	{
		Request r;
		QList<QByteArray> params;
		params<<"user_id=" + QByteArray::number(id);
		params<<"cursor=" + cursor;
		params<<"skip_status=true";
		params<<"include_user_entities=false";
		r.setURL(URL+ "?" +params[0] + "&" + params[1] + "&" + params[2]+ "&" + params[3]);
		OAuth::signRequest(params,r,URL,"GET");
		QByteArray response=r.get();

		// ���������� �����
		std::string json(response.data());
		_Object obj;
		int index=1;
		obj.parse(json,index);

		// ��������� ����� �� ������
		if ( isOk!=NULL && (obj.contains("errors") || !obj.contains("users")))
		{
			*isOk=false;
			return list;
		}

		cursor=((_String *)obj.getValue("next_cursor_str"))->value;

		// ������� ids � ������
		auto & arr=((_Mass *)obj.getValue("users"))->value;
		_Object * current;
		for (auto i=arr.cbegin();i!=arr.cend();++i)
		{
			current=(_Object*)(*i);
			Person person;
			decodeFromUtf16(((_String*)(current->getValue("name")))->value,person.name);	
			QString flag(person.name);
			person.id=QString(((_String*)(current->getValue("id_str")))->value).toULongLong();
			decodeFromUtf16(QString(((_String*)(current->getValue("profile_image_url")))->value).replace("\\/","/"),person.imgUrl);
			QString flag2(person.imgUrl);
			list<<person;
		}

	} while (cursor!="0");

	return list;
}

bool TwitterRequests::getStatus()
{
	if (!OAuth::isValidTokens())
		return false;

	QList<QByteArray> params;

	QByteArray URL="https://api.twitter.com/1.1/account/verify_credentials.json";

	Request r;
	OAuth::signRequest(params,r,URL,"GET",false);
	
	r.setURL(URL);
	QByteArray response=r.get();

	std::string json(response.data());
	_Object obj;
	int index=0;
	obj.parse(json,index);

	return !(obj.contains("errors"));
}

bool TwitterRequests::sendMessage(DirectMessage & message)
{
	if (message.senderID!=OAuth::USER_ID.toULongLong())
		return false;
	
	QByteArray text=QString(message.message).toLocal8Bit();

	QList<QByteArray> params;
	params<<"text=" + QUrl::toPercentEncoding(text);
	params<<"user_id=" + QByteArray::number(message.recipientID);

	QByteArray NEW_DIRECT_MESSAGE_URL="https://api.twitter.com/1.1/direct_messages/new.json";

	Request r;
	OAuth::signRequest(params,r,NEW_DIRECT_MESSAGE_URL,"POST",false);
	r.requestString="text=" + QUrl::toPercentEncoding(text) + "&user_id=" + QUrl::toPercentEncoding(QByteArray::number(message.recipientID));
	r.setURL(NEW_DIRECT_MESSAGE_URL);
	QByteArray response=r.post();

	// ���������� �����
	std::string json(response.data());
	_Object obj;
	int index=0;
	obj.parse(json,index);

	if (obj.contains("errors"))
	{		
		return false;
	}

	message.messageID=QString(((_String*)obj.getValue("id_str"))->value).toULongLong();

	QString date(((_String*)obj.getValue("created_at"))->value);

	message.date=getDateFromString(date);

	return true;
}


QList<DirectMessage> TwitterRequests::getLastMessages(qulonglong since_id,int count,bool * isOk)
{
	if (count>200)
		count=200;

	if (isOk!=0)
		*isOk=true;

	QList<DirectMessage> messages;

	QList<QByteArray> params;
	params<<"count=" + QByteArray::number(count);	
	if (since_id != 0)
		params<<"since_id=" + QByteArray::number(since_id);

	QByteArray URL="https://api.twitter.com/1.1/direct_messages.json";

	Request r;
	OAuth::signRequest(params,r,URL,"GET",false);
	URL+="?";
	for (int i=0;i<params.count();i++)
	{
		URL+=params[i];

		if (i!=params.count()-1)
			URL+="&";
	}
	r.setURL(URL);
	QByteArray response=r.get();

	

	std::string json(response.data());
	_Mass mass;
	_Object obj;
	int index=0;
	mass.parse(json,index);

	if (mass.value.size()==0)
	{
		*isOk= false;
		return messages;
	}

	index=0;
	obj.parse(json,index);

	if (isOk!=0 && obj.contains("errors"))
	{		
		*isOk= false;
		return messages;
	}

	auto & arr=mass.value;
	for (auto i=arr.cbegin();i!=arr.cend();++i)
	{
		DirectMessage dm;

		dm.messageID=QString(((_String*)((_Object*)(*i))->getValue("id_str"))->value).toULongLong();
		decodeFromUtf16(QString(((_String*)((_Object*)(*i))->getValue("text"))->value),dm.message);
		dm.senderID=QString(((_String*)((_Object*)(*i))->getValue("sender_id_str"))->value).toULongLong();
		dm.recipientID=QString(((_String*)((_Object*)(*i))->getValue("recipient_id_str"))->value).toULongLong();
		dm.date=getDateFromString(QString(((_String*)((_Object*)(*i))->getValue("created_at"))->value));
		
		messages<<dm;
	}
	
	return messages;	
}

QList<DirectMessage> TwitterRequests::getLastSentMessages(qulonglong since_id,int count ,bool * isOk)
{
	if (count>200)
		count=200;

	if (isOk!=0)
		*isOk=true;

	QList<DirectMessage> messages;

	QList<QByteArray> params;
	params<<"count=" + QByteArray::number(count);	
	if (since_id != 0)
		params<<"since_id=" + QByteArray::number(since_id);

	QByteArray URL="https://api.twitter.com/1.1/direct_messages/sent.json";

	Request r;
	OAuth::signRequest(params,r,URL,"GET",false);
	URL+="?";
	for (int i=0;i<params.count();i++)
	{
		URL+=params[i];

		if (i!=params.count()-1)
			URL+="&";
	}
	r.setURL(URL);
	QByteArray response=r.get();

	std::string json(response.data());
	_Mass mass;
	_Object obj;
	int index=0;
	mass.parse(json,index);

	if (mass.value.size()==0)
	{
		*isOk= false;
		return messages;
	}

	index=0;
	obj.parse(json,index);

	if (isOk!=0 && obj.contains("errors"))
	{		
		*isOk= false;
		return messages;
	}

	auto & arr=mass.value;
	for (auto i=arr.cbegin();i!=arr.cend();++i)
	{
		DirectMessage dm;

		dm.messageID=QString(((_String*)((_Object*)(*i))->getValue("id_str"))->value).toULongLong();
		decodeFromUtf16(QString(((_String*)((_Object*)(*i))->getValue("text"))->value),dm.message);
		dm.senderID=QString(((_String*)((_Object*)(*i))->getValue("sender_id_str"))->value).toULongLong();
		dm.recipientID=QString(((_String*)((_Object*)(*i))->getValue("recipient_id_str"))->value).toULongLong();
		dm.date=getDateFromString(QString(((_String*)((_Object*)(*i))->getValue("created_at"))->value));
		
		messages<<dm;
	}
	
	return messages;	
}

QList<DirectMessage> TwitterRequests::getSentMessagesBefore(qulonglong max_id,int count,bool * isOk)
{
	if (count>200)
		count=200;


	if (isOk!=0)
		*isOk=true;

	QList<DirectMessage> messages;

	QList<QByteArray> params;
	params<<"count=" + QByteArray::number(count);
	//params<<"include_entities=false";
	//params<<"skip_status=true";
	if (max_id != 0)
		params<<"max_id=" + QByteArray::number(max_id);

	QByteArray URL="https://api.twitter.com/1.1/direct_messages/sent.json";

	Request r;
	OAuth::signRequest(params,r,URL,"GET",false);
	URL+="?";
	for (int i=0;i<params.count();i++)
	{
		URL+=params[i];

		if (i!=params.count()-1)
			URL+="&";
	}
	r.setURL(URL);
	QByteArray response=r.get();

	std::string json(response.data());
	_Mass mass;
	_Object obj;
	int index=0;
	mass.parse(json,index);

	if (mass.value.size()==0)
	{
		*isOk= false;
		return messages;
	}


	index=0;
	obj.parse(json,index);

	if (isOk!=0 && obj.contains("errors"))
	{		
		*isOk= false;
		return messages;
	}

	auto & arr=mass.value;
	for (auto i=arr.cbegin();i!=arr.cend();++i)
	{
		DirectMessage dm;

		dm.messageID=QString(((_String*)((_Object*)(*i))->getValue("id_str"))->value).toULongLong();
		decodeFromUtf16(QString(((_String*)((_Object*)(*i))->getValue("text"))->value),dm.message);
		dm.senderID=QString(((_String*)((_Object*)(*i))->getValue("sender_id_str"))->value).toULongLong();
		dm.recipientID=QString(((_String*)((_Object*)(*i))->getValue("recipient_id_str"))->value).toULongLong();
		dm.date=getDateFromString(QString(((_String*)((_Object*)(*i))->getValue("created_at"))->value));
		
		messages<<dm;
	}
	
	return messages;	
}

QList<DirectMessage> TwitterRequests::getMessagesBefore(qulonglong max_id,int count,bool * isOk)
{
	if (count>200)
		count=200;


	if (isOk!=0)
		*isOk=true;

	QList<DirectMessage> messages;

	QList<QByteArray> params;
	params<<"count=" + QByteArray::number(count);
	//params<<"include_entities=false";
	//params<<"skip_status=true";
	if (max_id != 0)
		params<<"max_id=" + QByteArray::number(max_id);

	QByteArray URL="https://api.twitter.com/1.1/direct_messages.json";

	Request r;
	OAuth::signRequest(params,r,URL,"GET",false);
	URL+="?";
	for (int i=0;i<params.count();i++)
	{
		URL+=params[i];

		if (i!=params.count()-1)
			URL+="&";
	}
	r.setURL(URL);
	QByteArray response=r.get();

	std::string json(response.data());
	_Mass mass;
	_Object obj;
	int index=0;
	mass.parse(json,index);

	if (mass.value.size()==0)
	{
		*isOk= false;
		return messages;
	}

	index=0;
	obj.parse(json,index);

	if (isOk!=0 && obj.contains("errors"))
	{		
		*isOk= false;
		return messages;
	}

	auto & arr=mass.value;
	for (auto i=arr.cbegin();i!=arr.cend();++i)
	{
		DirectMessage dm;

		dm.messageID=QString(((_String*)((_Object*)(*i))->getValue("id_str"))->value).toULongLong();
		decodeFromUtf16(QString(((_String*)((_Object*)(*i))->getValue("text"))->value),dm.message);
		dm.senderID=QString(((_String*)((_Object*)(*i))->getValue("sender_id_str"))->value).toULongLong();
		dm.recipientID=QString(((_String*)((_Object*)(*i))->getValue("recipient_id_str"))->value).toULongLong();
		dm.date=getDateFromString(QString(((_String*)((_Object*)(*i))->getValue("created_at"))->value));
		
		messages<<dm;
	}
	
	return messages;	
}


DateTime TwitterRequests::getDateFromString(QString & string)
{
	QRegExp r_date("(\\w{3})\\s(\\w{3})\\s(\\d{2})\\s(\\d{2}):(\\d{2}):(\\d{2})\\s((\\+|-)\\d{4})\\s(\\d{4})");
	r_date.indexIn(string);		

	DateTime date;
	date.year=r_date.cap(9).toUShort();
	date.month=fromShortMonthName(r_date.cap(2));
	date.day=r_date.cap(3).toUShort();
	date.hour=r_date.cap(4).toUShort();
	date.min=r_date.cap(5).toUShort();
	date.sec=r_date.cap(6).toUShort();
	return date;
}

ushort TwitterRequests::fromShortMonthName(QString & name)
{
	QList<QString> l;

	l<<"Jan"<<"Feb"<<"Mar"<<"Apr"<<"May"<<"Jun"<<"Jul"<<"Aug"<<"Sep"<<"Oct"<<"Nov"<<"Dec";
	
	return l.indexOf(name)+1;
}