#ifndef TWITTERREQUESTS_H
#define TWITTERREQUESTS_H

#include <QObject>
#include "OAuth.h"
#include "Request.h"
#include "browser.h"
#include <qdesktopservices.h>
#include "Person.h"
#include "DirectMessage.h"
#include <stdlib.h>
#include "Parser\_Object.h"
#include "Parser\_String.h"
#include <qregexp.h>

/*!
	�����, �������������� �������� �������� ��������.

	��������!
		������ ��������� ������� �������� ����� ��������� ����������� (������ loginNewUser() � getOldTokens())

 */
class TwitterRequests : public QObject
{
	Q_OBJECT

public:
	TwitterRequests(QObject *parent);
	~TwitterRequests();

	/*!
		��������� ������������ �������
		\return ������������
	*/
	static bool getStatus();
		
	/*!
		��������� ���� ������ ������������
		\return ����������
	*/
	static bool loginNewUser();


	/*!
		��������� ������ ����������� ������������
		\return ����������
	*/
	static bool loadOldTokens();

	/*! 
		�������� ���������� � ������������� � id �������
		! �������� ! ��� ���������� ������ ������ ���� �������� ������
		param [out] isOk ���������� 
		\param [in] id 
		\return url
	 */
	static Person getUserInformation(bool * isOk=0,unsigned long long id=OAuth::USER_ID.toULongLong());


	/*! 
		�������� ��������� id ������
		! �������� ! ��� ���������� ������ ������ ���� �������� ������
		param [out] isOk ���������� 
		\param [in] id 
		\return ��������� id ������
	 */
	static QList<unsigned long long> getFrindIDs(bool * isOk=0,unsigned long long id=OAuth::USER_ID.toULongLong());


	/*! 
		�������� ��������� ������
		! �������� ! ��� ���������� ������ ������ ���� �������� ������
		\param [in] id 
		\param [out] isOk ���������� 
		\return ��������� ������
	 */
	static QList<Person> getFrinds(bool * isOk=0,unsigned long long id=OAuth::USER_ID.toULongLong());


	/*! 
		��������� ������ ���������
		! �������� ! ��� ���������� ������ ������ ���� �������� ������
		����� ������� ��������� ��������� � ������ ���� ������� � id ���������
		\param [in|out] message ���������
		\return ����������
	 */
	static bool sendMessage(DirectMessage & message);


	/*! 
		�������� ��������� ���������� ��������� �� ������ ������������� ������� � �������������
		! �������� ! ��� ���������� ������ ������ ���� �������� ������
		\param [in] since_id id ���������� ���������, � ������� �������� �� ����� ��������� ( 0 ���� �� �����)
		\param [in] count ���������� ��������� ( max 200)
		\param [out] isOk ����������
		\return ��������� ���������
	 */
	static QList<DirectMessage> getLastMessages(qulonglong since_id=0,int count=200 ,bool * isOk=0);

	/*! 
		�������� ��������� ������������ ��������� �������� ������������ ������� � �������������
		! �������� ! ��� ���������� ������ ������ ���� �������� ������
		\param [in] since_id id ���������� ���������, � ������� �������� �� ����� ��������� ( 0 ���� �� �����)
		\param [in] count ���������� ��������� ( max 200)
		\param [out] isOk ����������
		\return ��������� ���������
	 */
	static QList<DirectMessage> getLastSentMessages(qulonglong since_id=0,int count=200 ,bool * isOk=0);




	/*! 
		�������� ��������� ���������� ��������� �� ������ ������������� �� �������������
		! �������� ! ��� ���������� ������ ������ ���� �������� ������
		\param [in] max_id id ���������� ���������, �� �������� �� ����� ���������
		\param [in] count ���������� ��������� ( max 200)
		\param [out] isOk ����������
		\return ��������� ���������
	 */
	static QList<DirectMessage> getMessagesBefore(qulonglong max_id,int count=200,bool * isOk=0);

	/*! 
		�������� ��������� ������������ ��������� �������� ������������ �� �������������
		! �������� ! ��� ���������� ������ ������ ���� �������� ������
		\param [in] max_id id ���������� ���������, �� �������� �� ����� ���������
		\param [in] count ���������� ��������� ( max 200)
		\param [out] isOk ����������
		\return ��������� ���������
	 */
	static QList<DirectMessage> getSentMessagesBefore(qulonglong max_id,int count=200,bool * isOk=0);



private:	
	static void decodeFromUtf16(QString input, QChar * result);
	static ushort fromShortMonthName(QString & name);
	static DateTime getDateFromString(QString & string);
};

#endif // TWITTERREQUESTS_H
